import terminal

# initialize the terminal
terminal.init()

# enable the ability to hold a key instead of pressing and releasing it
terminal.enable_key_repeat()

while True:
	# remember where we were
	x, y = terminal.get_xy()
	# print the yellow cursor here
	terminal.set_foreground(255, 255, 0)
	terminal.print("_")
	# reset the color afterwards
	terminal.set_foreground(255, 255, 255)
	# wait for the user input
	inp = terminal.waitkey()
	# go back to where we were
	terminal.set_xy(x, y)
	# print space (erase the cursor)
	terminal.print(" ")
	# go back to where we were once again
	terminal.set_xy(x, y)
	# if user pressed enter...
	if inp == "enter":
		# goto next line
		terminal.print('\n')
		# reset to the line start
		terminal.print('\r')
	# if user pressed backspace...
	elif inp == "backspace":
		# go left
		if x > 0:
			x -= 1
		terminal.set_xy(x, y)
	# if any other "good" key
	elif len(inp) == 1:
		# print it
		terminal.print(inp)

