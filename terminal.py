"""
The terminal is a 80*25 grid where each cell holds exactly 1 character.
Each cell has its own text color and background color.
"""

from typing import Tuple

import pygame
import sys
import time
import random
import os


from pygame.locals import *


PLACE_WIDTH = 0
PLACE_HEIGHT = 0
PLACES_WIDTH = 0
PLACES_HEIGHT = 0

SCREEN_WIDTH, SCREEN_HEIGHT = None, None

TAB_SIZE = 4

background_color = (0, 0, 0)
foreground_color = (255, 255, 255)
cursor_x = 0
cursor_y = 0


def init(width: int=80, height: int=25, font_size: int=20) -> None:
	"""
	Initializes the terminal.
	@:param width sets the horizontal "resolution" of the terminal - the number of characters in each row
	@:param height sets the vertical "resolution" of the terminal - the number of character in each column
	@:param font_size sets the base font size. Its a good idea not change the default 20 here
	"""
	global screen, font
	global PLACE_HEIGHT, PLACE_WIDTH, SCREEN_WIDTH, SCREEN_HEIGHT
	global PLACES_WIDTH, PLACES_HEIGHT
	pygame.init()
	font = pygame.font.Font(os.path.join(os.path.dirname(__file__), "consola.ttf"), font_size)
	PLACES_WIDTH = width
	PLACES_HEIGHT = height
	PLACE_WIDTH, _ = font.size("█")
	PLACE_HEIGHT = font.get_ascent() - font.get_descent()
	SCREEN_WIDTH, SCREEN_HEIGHT = PLACES_WIDTH * PLACE_WIDTH, PLACE_HEIGHT * PLACES_HEIGHT
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)


def get_width() -> int:
	"""
	Returns the horizontal size of the terminal
	"""
	return PLACES_WIDTH


def get_height() -> int:
	"""
	Returns the vertical size of the terminal
	"""
	return PLACES_HEIGHT


def get_xy() -> Tuple[int, int]:
	"""
	Returns a pair of numbers - the position of the cursor (top left is 0,0; bottom right is 79, 24).
	"""
	return cursor_x, cursor_y


def set_xy(x: int, y: int) -> None:
	"""
	Moves the cursor to the specified position (top left is 0,0; bottom right is 79, 24).
	"""
	if x < 0 or y < 0 or x >= PLACES_WIDTH or y >= PLACES_HEIGHT:
		raise RuntimeError("invalid goto position")

	global cursor_x, cursor_y
	cursor_x = x
	cursor_y = y


def set_foreground(red: int, green: int, blue: int) -> None:
	"""
	Sets the current text color.
	This color will be applied to all characters printed afterwards.
	"""
	global foreground_color
	foreground_color = (red, green, blue)


def set_background(red: int, green: int, blue: int) -> None:
	"""
	Sets the current background color.
	This color will be applied to all characters printed afterwards.
	"""
	global background_color
	background_color = (red, green, blue)


def print(s: str) -> None:
	"""
	Prints a string, moving right (or to the beginning of the next line) after every
	printed character.
	"""
	for ch in s:
		put(ch)


def put(c: str) -> None:
	"""
	Prints exactly one character and moves the cursor to the next position.
	If the cursor moves past the last cell, it is reset to the very first (top left)
	cell of the terminal.
	"""
	if not isinstance(c, str):
		raise RuntimeError("print argument is not a string")
	if len(c) != 1:
		raise RuntimeError("print argument is not a single character")

	global cursor_x, cursor_y

	if c == '\t':
		for i in range(TAB_SIZE):
			print(' ')
		return

	if c == '\n':
		cursor_y += 1
		if cursor_y >= PLACES_HEIGHT:
			cursor_y = 0
		return

	if c == '\r':
		cursor_x = 0
		return

	surface = pygame.display.get_surface()
	surface.fill(background_color, (cursor_x * PLACE_WIDTH, cursor_y * PLACE_HEIGHT, PLACE_WIDTH, PLACE_HEIGHT))

	if c == '\0' or ord(c) > 0xffff:
		s = '�'
	else:
		s = c

	text = font.render(s, True, foreground_color, (0, 0, 0, 0))
	textpos = text.get_rect()
	textpos.center = (cursor_x * PLACE_WIDTH + PLACE_WIDTH // 2, cursor_y * PLACE_HEIGHT + PLACE_HEIGHT // 2)
	surface.blit(text, textpos, special_flags=BLEND_ADD)

	cursor_x += 1
	if cursor_x >= PLACES_WIDTH:
		cursor_y += 1
		cursor_x = 0
		if cursor_y >= PLACES_HEIGHT:
			cursor_y = 0


_key_decode = {
	K_UP: "up",
	K_DOWN: "down",
	K_LEFT: "left",
	K_RIGHT: "right",
	K_LSHIFT: "shift_left",
	K_RSHIFT: "shift_right",
	K_LCTRL: "ctrl_left",
	K_RCTRL: "ctrl_right",
	K_LALT: "alt_left",
	K_RALT: "alt_right",
	K_ESCAPE: "escape",
	K_RETURN: "enter",
	K_DELETE: "delete",
	K_BACKSPACE: "backspace",
	K_F1: "F1",
	K_F2: "F2",
	K_F3: "F3",
	K_F4: "F4",
	K_F5: "F5",
	K_F6: "F6",
	K_F7: "F7",
	K_F8: "F8",
	K_F9: "F9",
	K_F10: "F10",
	K_F11: "F11",
	K_F12: "F12",
}


def disable_key_repeat() -> None:
	"""
	Disables key repetitions.
	Keys pressed by the user will only issue a single event.
	"""
	pygame.key.set_repeat()


def enable_key_repeat(delay: int = 300, interval: int = 50) -> None:
	"""
	Enables key repetition.
	When a key is being held, it will repeat its event every interval milliseconds, starting from delay milliseconds.
	"""
	pygame.key.set_repeat(delay, interval)


def waitkey() -> str:
	"""
	Waits for user input and returns the name of the pressed key.
	"""
	pygame.display.update()
	while True:
		event = pygame.event.wait()
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
		elif event.type == KEYDOWN:
			if event.key in _key_decode:
				return _key_decode[event.key]
			return event.unicode


def is_shift() -> bool:
	"""
	Returns True if a shift key is currently being held
	"""
	return pygame.key.get_mods() & pygame.KMOD_SHIFT


def is_ctrl() -> bool:
	"""
	Returns True if a ctrl key is currently being held
	"""
	return pygame.key.get_mods() & pygame.KMOD_CTRL


def is_alt() -> bool:
	"""
	Returns True if an alt key is currently being held
	"""
	return pygame.key.get_mods() & pygame.KMOD_ALT




