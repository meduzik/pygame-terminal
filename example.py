import terminal
import random


# The terminal is a 80*25 grid where each cell holds exactly 1 character.
# Each cell has its own text color and background color.
terminal.init()

# Prints exactly one character and moves the cursor to the next position.
# If the cursor moves past the last cell, it is reset to the very first (top left)
# cell of the terminal.
terminal.put('X')

# Moves the cursor to the specified position (top left is 0,0; bottom right is 79, 24).
terminal.set_xy(2, 5)

# Sets the current text color.
# This color will be applied to all characters printed afterwards.
terminal.set_foreground(255, 255, 0)

# Sets the current background color.
# This color will be applied to all characters printed afterwards.
terminal.set_background(128, 0, 128)

# Prints a string, moving right (or to the beginning of the next line) after every
# printed character.
# Terminal works with unicode too.
terminal.print("Привет!\n\r")

while True:
	# Waits for user input and returns the name of the pressed key.
	key = terminal.waitkey()
	terminal.set_background(random.randrange(256), random.randrange(256), random.randrange(256))
	terminal.print(key)


